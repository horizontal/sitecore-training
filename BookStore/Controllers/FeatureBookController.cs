﻿using BookStore.Mapper.sitecore.templates.Book;
using Glass.Mapper.Sc.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class FeatureBookController : Controller
    {
        // GET: FeatureBook
        public ActionResult index()
        {

            var mvcContext = new MvcContext();
            var item = mvcContext.GetDataSourceItem<Book_Detail>();

            return View(item);
        }
    }
}