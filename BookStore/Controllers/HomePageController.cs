﻿using BookStore.Mapper.sitecore.templates.Book;
using BookStore.Mapper.sitecore.templates.Global;
using Glass.Mapper.Sc.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class HomePageController : Controller
    {
        // GET: Home
        public ActionResult CreateHome()
        {
            var mvcContext = new MvcContext();
            var item = mvcContext.GetContextItem<Home>();

            return View(item);
        }
    }
}