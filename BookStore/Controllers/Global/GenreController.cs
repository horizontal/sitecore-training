﻿using BookStore.Mapper.sitecore.templates.Book;
using Glass.Mapper.Sc.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers.Global
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult CreateGenre()
        {
            var mvcContext = new MvcContext();
            var item = mvcContext.GetContextItem<Book_Genre_Detail>();
            var count = 0;
            foreach(var book in item.book_Detail)
            {
                book.serialid = count + 1;
                count = count + 1;
            }
            return View(item);
           
        }
    }
}