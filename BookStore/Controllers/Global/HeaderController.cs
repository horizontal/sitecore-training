﻿using BookStore.Mapper.sitecore.templates.Global;
using Glass.Mapper.Sc.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers.Global
{
    public class HeaderController : Controller
    {
        // GET: Header
        public ActionResult AddHeader()
        {

            var mvcContext = new MvcContext();
       
            var item = mvcContext.GetDataSourceItem<Header>();


            ViewBag.add = "";
            return View(item);
        }

        public ActionResult errorinfo()
        {
            var mvcContext = new MvcContext();
            var item = mvcContext.GetContextItem<ErrorPage>();
            return View(item);
        }

        
    }
}