﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Mapper.sitecore.templates.Book

{
    public partial class Book_Genre_Detail
    {
        [SitecoreChildren]
        public virtual IEnumerable<Book_Detail> book_Detail { get; set; }
    }
}