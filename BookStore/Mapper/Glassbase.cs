﻿using Glass.Mapper.Sc.Configuration;
using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Mapper
{
    public abstract partial class GlassBase
    {
        [SitecoreInfo(SitecoreInfoType.Path)]
        public virtual string Path { get; }
        [SitecoreInfo(SitecoreInfoType.Name)]
        public virtual string Name { get; set; }
        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        public virtual string DisplayName { get; set; }
        [SitecoreField("__Style")]
        public virtual string Style { get; set; }
        [SitecoreInfo(SitecoreInfoType.Url)]
        public virtual string Url { get; set; }
        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        public virtual ID TemplateId { get; set; }
        [SitecoreField("__Created")]
        public virtual DateTime Created { get; set; }
    }
    public partial interface IGlassBase
    {
        [SitecoreInfo(SitecoreInfoType.Path)]
        string Path { get; }
        [SitecoreInfo(SitecoreInfoType.Name)]
        string Name { get; set; }
        [SitecoreInfo(SitecoreInfoType.DisplayName)]
        string DisplayName { get; set; }
        [SitecoreInfo(SitecoreInfoType.Url)]
        string Url { get; set; }
        [SitecoreInfo(SitecoreInfoType.TemplateId)]
        ID TemplateId { get; set; }
        [SitecoreField("__Style")]
        string Style { get; set; }
        [SitecoreField("__Created")]
        DateTime Created { get; set; }
    }
}
