﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Mapper.sitecore.templates.Book
{
    public partial class Book_Detail
    {
        [SitecoreItem]
        public virtual Item bookitem { get; set; }
        public int serialid { get; set; }
    }
}