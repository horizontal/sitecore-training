﻿
using Sitecore.Configuration;
using Sitecore.Pipelines.HttpRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Mapper
{
    
    public class Error404 : HttpRequestProcessor

    {


        public override void Process(HttpRequestArgs args)
        {
            if (Sitecore.Context.Item != null || Sitecore.Context.Site == null || Sitecore.Context.Database == null)
            {

                return;
            }
            if (Sitecore.Context.Item == null)
            {
                var notFoundItem = Sitecore.Context.Database.GetItem(Settings.ItemNotFoundUrl);
                if (notFoundItem != null)
                {
                   
                    Sitecore.Context.Item = notFoundItem;
                }
            }
        }



    }
}